
Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		
		Babele.get().register({
			module: 'FoundryVTT-pf2-it',
			lang: 'it',
			dir: 'compendium'
		});
	}
});